import React from 'react';
import { Link } from 'react-router-dom';
// import SignLinks from './SignLinks';
import useAuth from '../hooks/auth.hook';

const Header = () => {
  const { logout } = useAuth();
  return (
    <header>
      <Link className='homelink' to='/'>Home</Link>
      <button className='signlink' onClick={logout}>Log out</button >
      {/* <SignLinks logOut={props.logOut} /> */}
    </header>
  )
}

export default Header;
