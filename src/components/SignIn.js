import React, { useRef } from 'react';
import { Link, useHistory } from 'react-router-dom';
import useForm from '../hooks/form.hook';
import useValidation from '../hooks/validation.hook';
import validationLoginSchema from '../validation/validationLoginSchema';
import useAuth from '../hooks/auth.hook';

const SignIn = () => {
  const inputRef = useRef(null);
  const { login } = useAuth();
  const { inputs, handleInputChange } = useForm({
    email: '',
    password: ''
  });
  const { errors, validate, clearErrors } = useValidation(inputs, validationLoginSchema);

  const history = useHistory();

  const handleSubmit = (event) => {
    event.preventDefault();
    const response = validate();
    if(Object.keys(response).length) {
      inputRef.current.blur();
      return;
    }
    // login('3e23e23e');
    history.push('/');
  }

  const clearFocusHandler = (field) => clearErrors(field);
  const checkAbility = () => Object.values(inputs).every(v => !!v);

  return (
    <>
      <form className='signform' onSubmit={handleSubmit}>
        <h2 className='signform__title'>Welcome back!</h2>

        <div className='signform__group'>
          <p>Not a member yet?</p>
          <Link className='signform__button' to='/signup'>Sign up</Link>
        </div>

        <input
          ref={inputRef}
          type='text'
          name='email'
          value={inputs.email}
          className='signform__input'
          placeholder='Email'
          autoComplete='off'
          onFocus={() => clearFocusHandler('email')}
          onChange={(event) => handleInputChange(event.target.value, 'email')}
        />

      <p className='signform__message'>{errors.email}</p>

        <input
          type='password'
          name='password'
          value={inputs.password}
          className='signform__input'
          placeholder='Password'
          onChange={(event) => handleInputChange(event.target.value, 'password')}
        />

      <p className='signform__message'>{errors.password}</p>

        <button
          disabled={!checkAbility()}
          className='signform__button'
          type='submit'
        >
          Sign in
          </button>
      </form>
    </>
  )
}

export default SignIn;
