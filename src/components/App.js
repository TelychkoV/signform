import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import '../styles/App.css';
import useRoutes from '../hooks/routes.hook';
import useAuth from '../hooks/auth.hook';
import Header from '../components/Header';

const App = () => {
  const { token } = useAuth();
  const isAuthenticated = !!token;
  const routes = useRoutes(isAuthenticated);

  return (
    <Router>
      <div className='container'>
          <Header />
          {routes}
      </div>
    </Router>
  )
}

export default App;


