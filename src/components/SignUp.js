import React, { useRef } from 'react';
import { Link, useHistory } from 'react-router-dom';
import useForm from '../hooks/form.hook';
import useValidation from '../hooks/validation.hook';
import validationLoginSchema from '../validation/validationLoginSchema';

const SignUp = () => {
  const inputRef = useRef(null);
  const { inputs, handleInputChange } = useForm({
    firstName: '',
    lastName: '',
    email: '',
    password: ''
  });
  const { errors, validate, clearErrors } = useValidation(inputs, validationLoginSchema);

  const history = useHistory();


  const handleSubmit = (event) => {
    event.preventDefault();
    const response = validate();
    if(Object.keys(response).length) {
      inputRef.current.blur();
      return;
    }
    history.push('./signin');
  }

  const clearFocusHandler = (field) => clearErrors(field);
  const checkAbility = () => Object.values(inputs).every(v => !!v);

  return (
    <>
      <form className='signform' onSubmit={handleSubmit}>
        <h2 className='signform__title'>Welcome!</h2>

        <div className='signform__group'>
          <p>Already a member?</p>
          <Link className='signform__button' to='/signin'>Sign in</Link>
        </div>

        <input
        ref={inputRef}
          type='text'
          name='firstName'
          className='signform__input'
          placeholder='First name'
          onFocus={() => clearFocusHandler('firstName')}
          onChange={(event) => handleInputChange(event.target.value, 'firstName')}
        />

<p className='signform__message'>{errors.firstName}</p>

        <input
        ref={inputRef}
          type='text'
          name='lastName'
          className='signform__input'
          placeholder='Last name'
          onFocus={() => clearFocusHandler('lastName')}
          onChange={(event) => handleInputChange(event.target.value, 'lastName')}
        />

<p className='signform__message'>{errors.lastName}</p>
        <input
        ref={inputRef}
          type='text'
          name='email'
          className='signform__input'
          placeholder='Email'
          onFocus={() => clearFocusHandler('email')}
          onChange={(event) => handleInputChange(event.target.value, 'email')}
        />
        <p className='signform__message'>{errors.email}</p>

        {/* {error ?
          <p className='signform__message'>This email is already in use.</p> : null
        } */}

        <input
        ref={inputRef}
          type='password'
          name='password'
          className='signform__input'
          placeholder='Password'
          onFocus={() => clearFocusHandler('password')}
          onChange={(event) => handleInputChange(event.target.value, 'password')}
        />

<p className='signform__message'>{errors.password}</p>

        <button
        disabled={!checkAbility()}
          className='signform__button'
          type='submit'
        >
          Sign up
          </button>
      </form>
    </>
  )
}

export default SignUp;
