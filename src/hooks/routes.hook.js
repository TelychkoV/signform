import React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import '../styles/App.css';
import Home from '../components/Home';
import SignIn from '../components/SignIn';
import SignUp from '../components/SignUp';

const useRoutes = (isAuthenticated) => {
    if(isAuthenticated) {
        return (
            <Switch>
                <Route exact path='/' component={Home} />
                <Redirect to='/' />
            </Switch>
        )
    } 

    return (
        <Switch>
            <Route exact path='/signin' component={SignIn} />
            <Route exact path='/signup' component={SignUp} />
            <Redirect to='/signin' />
        </Switch>
    )
}

export default useRoutes;