import {useState, useCallback} from 'react';

const useValidation = (data, schema) => {
    const [errors, setErrors] = useState({});

    const validate = useCallback(() => {
        let prop, errors = {};
        for(prop in data) {
            if(data.hasOwnProperty(prop)) {
                if(prop in schema) {
                    if(schema[prop](data[prop])) {
                        errors[prop] = schema[prop](data[prop]);
                    } else {
                        delete errors[prop];
                    }
                }
            }
        }
        
        setErrors(errors);
        return errors;
    }, [data]);

    const clearErrors = key => setErrors(data => ({ ...data, [key]: ''}))

    return { errors, validate, clearErrors };
}

export default useValidation;