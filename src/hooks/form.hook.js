import {useState} from 'react';

const useForm = (data) => {
    const [inputs, setInputs] = useState(data);

    const handleInputChange = (value, name) => {
        setInputs((formData) => ({
            ...formData,
            [name]: value
        }))
    }

    return { inputs, handleInputChange };
}

export default useForm;