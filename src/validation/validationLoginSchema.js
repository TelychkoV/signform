const validationUserSchema = {
    email: (value) => {
        if(!value) {
            return 'Required Email'
        }

        if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(value)) {
            return 'Invalid email address';
        }
    },

    password: (value) => {
        if(!value) {
            return 'Required Password'
        }

        if(value.length < 4) {
            return 'Password must be at least 4 symbols'
        }
    },

    firstName: (value) => {
        if(!value) {
            return 'Required FirstName'
        }
    },

    lastName: (value) => {
        if(!value) {
            return 'Required LastName'
        }
    }
}

export default validationUserSchema;